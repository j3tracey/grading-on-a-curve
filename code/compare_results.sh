#!/bin/bash

issuesFile="$1"
resultsFile="$2"
bugzillaDir="$3/bugs/"
bugToIntroDir="$4"

fixes=$(jq 'map(.[0]) | unique' "$resultsFile" | grep -Eo '[[:alnum:]]+')
for fix in $fixes ; do
    bug=$(jq "to_entries[] | select(.value.\"hash\" == \"$fix\") | .key" "$issuesFile" | tr -dc [0-9])
    blamed=$(jq "unique | .[] | select(.[0] == \"$fix\")[1]" "$resultsFile" | tr -d '"')
    if [ -f "$bugzillaDir/$bug.json" ] ; then
        groundTruth=$(jq '.inducedBy | map(.revisions)[]' "$bugzillaDir/$bug.json" | grep -Eo '[[:alnum:]]+')
        match=0
        for commit in $blamed ; do
            if grep -q "$commit" <(echo $groundTruth) ; then ((match++)) ; fi
        done
        szz=$(echo "$blamed" | wc -w)
        form=$(echo "$groundTruth" | wc -w)
        echo "$bug,$fix,$szz,$match,$form"
        echo "$groundTruth" > "$bugToIntroDir/$bug"
    else
        echo "$blamed" > "$bugToIntroDir/$bug"
    fi
done
