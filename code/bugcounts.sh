#!/bin/sh
projects="$1"
issuesDir="$2"
tablesDir="$3"

for p in $projects; do
    f="$issuesDir/$p-issues/res0.json"
    #echo $f
    #jq '.issues | length' "$f"
    if [ "$(jq '.issues | length' "$f")" -gt 0 ]; then
        allIssues="$(jq '.issues | .[] | .key' "$f" | tr -d '"-' | sort | uniq)"
        blamedIssues="$(cut -d, -f2 "$tablesDir/$p" | sort | uniq)"
        echo $allIssues
        echo $blamedIssues
        allIssuesCount=$(echo "$allIssues" | grep -v '^$' | wc -l)
        blamedIssuesCount=$(echo "$blamedIssues" | grep -v '^$' | wc -l)
        echo "$p,$allIssuesCount,$blamedIssuesCount"
    else
        echo "$p,0,0"
    fi
done
