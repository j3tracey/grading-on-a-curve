#!/usr/bin/env python3

import sys
import matplotlib.pyplot as plt
import matplotlib as mpl

import vcclib


def run(args):
    git_dirs = args[0].split(':')
    # the paths in the git dir to filter on (use "" or . to use everything)
    project_paths = args[1].split(':')
    # the directory where experiences are stored
    exp_dirs = vcclib.expdirs(args[2].split(':'))

    counts = vcclib.count_all_commits(git_dirs, project_paths, exp_dirs, {})
    cuml_tot = [sum(c.total for c in counts[:j+1]) for j in range(len(counts))]
    return cuml_tot


def main(argv):
    # the path+name of where to save the resulting plot
    frac_path = argv[9]
    tot_path = argv[10]

    cuml_tot1 = run(argv[1:4])
    label1 = argv[4]
    cuml_tot2 = run(argv[5:8])
    label2 = argv[8]

    cuml_frac1 = [v/cuml_tot1[-1] for v in cuml_tot1]
    cuml_frac2 = [v/cuml_tot2[-1] for v in cuml_tot2]
    xs1 = [x+1 for x in range(len(cuml_tot1))]
    xs2 = [x+1 for x in range(len(cuml_tot2))]

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=18)
    mpl.rcParams["text.latex.preamble"] = \
        "\\usepackage{relsize}\n\\usepackage{xspace}"
    l1 = plt.plot(xs1, cuml_frac1, 'b+',
                  label=r"{} $c_{{\le j}}$".format(label1))
    l2 = plt.plot(xs2, cuml_frac2, 'rs',
                  label=r"{} $c_{{\le j}}$".format(label2))
    plt.xlabel("$j=$ Experience")
    plt.ylabel("Fraction of projects' commits")
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(left=1)
    ax = plt.gca()
    plt.legend(loc='lower right')
    plt.tight_layout()
    plt.savefig(frac_path)

    l1.pop().remove()
    l2.pop().remove()

    plt.ylabel("Number of projects' commits")
    ax.set_ylim([1, 50000])
    plt.plot(xs1, cuml_tot1, 'b+', label=r"{} $c_{{\le j}}$".format(label1))
    plt.plot(xs2, cuml_tot2, 'rs', label=r"{} $c_{{\le j}}$".format(label2))
    plt.tight_layout()
    plt.savefig(tot_path)


if __name__ == '__main__':
    main(sys.argv)
