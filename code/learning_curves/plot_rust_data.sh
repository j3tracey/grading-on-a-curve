DATA_DIR=/home/j3tracey/Documents/rustsafety/data/
CODE_DIR=/home/j3tracey/Documents/rustsafety/code/

# CSS styling
# stylo
cd "$DATA_DIR/servo"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "." "components/style/" "$DATA_DIR/learning-curve-plots/stylo.svg" 4

# Rendering
# webrender
cd "$DATA_DIR/webrender"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "../gecko-dev:." "gfx/webrender_bindings:." "$DATA_DIR/learning-curve-plots/webrender.svg" 2

# Color management
# qcms
cd "$DATA_DIR/qcms"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "." "." "$DATA_DIR/learning-curve-plots/qcms-rust.svg" 0

# MP4 Parser
# mp4parse-rust
cd "$DATA_DIR/mp4parse-rust"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "." "." "$DATA_DIR/learning-curve-plots/mp4parse-rust.svg" 0

# Unicode Encoder
# encoding_rs
cd "$DATA_DIR/encoding_rs"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "." "." "$DATA_DIR/learning-curve-plots/encoding_rs.svg" 0

# Combined (above + Libcubeb: MacOS)
cd "$DATA_DIR/cubeb-coreaudio-rs"
python3 "$CODE_DIR/author-identities.py" . | sort > .mailmap
python3 "$CODE_DIR/learning_curves/plot_T1s.py" "$DATA_DIR/gecko-dev:$DATA_DIR/servo:$DATA_DIR/webrender:$DATA_DIR/qcms:$DATA_DIR/mp4parse-rust:$DATA_DIR/encoding_rs:$DATA_DIR/cubeb-coreaudio-rs" "gfx/webrender_bindings:components/style/:::::" "$DATA_DIR/learning-curve-plots/rust-combined.svg" 7
