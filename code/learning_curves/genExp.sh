#!/bin/bash

gitDir="$1"
recentCommit="$2"
experienceDir="$3"
if [ ! -z "$(ls -A "$experienceDir")" ] ; then
   echo "experience dir is not empty: $experienceDir"
   exit 1
fi
count=0
echo "counting commits..."
maxCount=$(git -C "$gitDir" log --full-history --no-merges --use-mailmap --format='format:' "$recentCommit" | wc -l)
(git -C "$gitDir" log --full-history --reverse --no-merges --use-mailmap --format='format:%ct	%H	%aN <%aE>' "$recentCommit" | sort -n | cut -f2,3; echo) | while IFS= read commit
do
    echo -ne "commit $count / $maxCount\r"
    #echo "commit: $commit"
    author=$(echo "$commit" | cut -f2 | tr '/' '_')
    #echo "author: $author"
    commit_hash=$(echo "$commit" | cut -f1)
    if [ ! -z "$author" ] ; then
        echo "$commit_hash,$count" >> "$experienceDir/$author"
    fi
    count=$(($count + 1))
done
echo
