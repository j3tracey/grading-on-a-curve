#!/bin/bash

# $1 is issues/${foo}/res0.json
# $2 is $bugIntroDir
# $3 is the firefox repo dir

bugs=$(jq -r '.issues | map(.key) | .[]' "$1" | tr -d '-')
cd "$2"
for bug in $bugs ; do
    #echo $bug
    if [ -f "$bug" ] ; then
        while read vcc ; do
            if [ -z "$vcc" ]; then
                echo "ERROR: empty introducer file: $bug"
                echo "(continuing best we can, but you need to fix a bug somewhere)"
                continue
            fi
            if [ -z "$(git  -C "$3" log --since-as-filter=2012-04-09 -1 "$vcc")" ]; then
                continue
            fi
            contribto=$(grep -l $vcc *)
            echo "$vcc",$contribto | tr ' ' ,
        done < $bug
    fi
done
