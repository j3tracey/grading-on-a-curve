#!/usr/bin/env python3

import sys
from decimal import Decimal
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl

import vcclib

# n.b.: ell is l spelled out to prevent confusion with the 1 character


def main(argv):
    argv2 = argv[1:]
    plot_path = argv2.pop(0)

    model1_name = argv2.pop(0)
    model1_t1 = Decimal(argv2.pop(0))
    model1_ell = Decimal(argv2.pop(0))
    model1_t1_err_low = Decimal(argv2.pop(0))
    model1_ell_err_low = Decimal(argv2.pop(0))
    model1_t1_err_up = Decimal(argv2.pop(0))
    model1_ell_err_up = Decimal(argv2.pop(0))

    model2_name = argv2.pop(0)
    model2_t1 = Decimal(argv2.pop(0))
    model2_ell = Decimal(argv2.pop(0))
    model2_t1_err_low = Decimal(argv2.pop(0))
    model2_ell_err_low = Decimal(argv2.pop(0))
    model2_t1_err_up = Decimal(argv2.pop(0))
    model2_ell_err_up = Decimal(argv2.pop(0))

    max_j = int(argv2.pop(0))

    model1_t1_sig = vcclib.sigfigs([model1_t1,
                                    model1_t1_err_low,
                                    model1_t1_err_up])[0]
    model1_ell_sig = vcclib.sigfigs([model1_ell,
                                     model1_ell_err_low,
                                     model1_ell_err_up])[0]
    model2_t1_sig = vcclib.sigfigs([model2_t1,
                                    model2_t1_err_low,
                                    model2_t1_err_up])[0]
    model2_ell_sig = vcclib.sigfigs([model2_ell,
                                     model2_ell_err_low,
                                     model2_ell_err_up])[0]

    model1_t1_str = np.format_float_positional(model1_t1_sig, 3,
                                               fractional=False)
    model1_ell_str = np.format_float_positional(-model1_ell_sig, 3,
                                                fractional=False)
    model2_t1_str = np.format_float_positional(model2_t1_sig, 3,
                                               fractional=False)
    model2_ell_str = np.format_float_positional(-model2_ell_sig, 3,
                                                fractional=False)

    xs = [x+1 for x in range(max_j)]

    ys_model1 = [model1_t1 * (x)**-model1_ell for x in xs]
    ys_model1_low = [model1_t1_err_low * (x)**-model1_ell_err_low for x in xs]
    ys_model1_up = [model1_t1_err_up * (x)**-model1_ell_err_up for x in xs]

    ys_model2 = [model2_t1 * (x)**-model2_ell for x in xs]
    ys_model2_low = [model2_t1_err_low * (x)**-model2_ell_err_low for x in xs]
    ys_model2_up = [model2_t1_err_up * (x)**-model2_ell_err_up for x in xs]

    plt.rc('text', usetex=True)
    plt.rc('font', family='serif', size=18)
    mpl.rcParams["text.latex.preamble"] = \
        "\\usepackage{relsize}\n\\usepackage{xspace}"
    plt.plot(xs, ys_model1, 'b--',
             label=r"{}: $P_j={} j^{{{}}}$".format(
                 model1_name, model1_t1_str, model1_ell_str))
    plt.fill_between(xs, ys_model1_low, ys_model1_up,
                     color='blue', alpha=0.2)
    plt.plot(xs, ys_model2, 'r-',
             label=r"{}: $P_j={} j^{{{}}}$".format(
                 model2_name, model2_t1_str, model2_ell_str))
    plt.fill_between(xs, ys_model2_low, ys_model2_up,
                     color='red', alpha=0.2)
    plt.xlabel("$j=$ Experience")
    plt.ylabel("$P_j$")
    plt.xlim(left=1)
    plt.legend(loc="upper right")
    plt.tight_layout()
    plt.savefig(plot_path)


if __name__ == '__main__':
    main(sys.argv)
