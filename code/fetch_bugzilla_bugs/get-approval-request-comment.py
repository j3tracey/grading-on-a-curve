from urllib3 import PoolManager
import json
import sys


if __name__ == '__main__':
    bugzillaID = sys.argv[1]
    bugzilla_project_name = 'bugzilla.mozilla.org'
    request = 'https://{}/rest/bug/{}/comment'.format(bugzilla_project_name,
                                                      bugzillaID)
    introducerIDs = set()
    res = PoolManager().request('GET', request, retries=10)
    j = json.loads(res.data)
    for comment in j['bugs'][bugzillaID]['comments']:
        for line in comment['text'].split('\n'):
            if '[Feature' in line:
                introducerIDs |= {word for word in line.
                                  replace(',', ' ').
                                  replace(';', ' ').
                                  replace(':', ' ').
                                  replace('#', ' ').split()
                                  if word.isdigit() and int(word) > 1000}
    if len(introducerIDs) > 0:
        print(json.dumps(list(introducerIDs)))
