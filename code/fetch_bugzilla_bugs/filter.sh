#!/bin/bash

## Filters the result of Bugzilla queries.
## Intended to be modified directly, args can only specify the paths of the
## project root and the Firefox clone.

## Most of these projects turn our to have 0 security bugs, so this ends up
## being a reproducible way to create empty lists.

## To find out if a file was moved in a git history, check the output of:
# git log --diff-filter=A -- $filepath
## and
# git diff-tree --no-commit-id --name-only -r $commitThatAddedFileAbove
## (these work even if the file has since been removed)

codeDir="$1"
issuesDir="$2"
repoDir="$3"
toFilter="$issuesDir/to_filter/"

getIssues () {
    for name in "$@" ; do
        jsonFile="$name/res0.json"
        jq '.issues | .[] | .key' "$jsonFile" | tr -d '"-'
    done | sort -n | uniq
}


# only include bugs whose fixes touch at least one file in $2:...
filter () {
    project="$1"
    echo "filtering $project" >&2
    issues=$(getIssues "$project")
    shift
    matches=$(
        for issue in $issues ; do
            echo "checking bug $issue" >&2
            affectedFiles=$(python3 "$codeDir/fetch_bugzilla_bugs/get_bugzilla_patches.py" 'files' $issue)
            match=$(for filteredFile in $@ ; do echo $affectedFiles | grep "$filteredFile" ; done)
            if [ -n "$match" ] ; then
                echo "$issue"
                continue
            fi
        done)
    echo '{"issues": ['
    unset comma
    for match in $matches ; do
        if [ -n "$comma" ]; then echo ","; fi
        jq ".issues | map(select(.key == \"-$match\")) | .[]" "$project/res0.json"
        comma=true
    done
    echo ']}'
}

# only include bugs whose fixes touch at least one file *not* in the tree as of commit $2
inverse_filter() {
    project="$1"
    echo "(inverse) filtering $project" >&2
    issues=$(getIssues "$project")
    shift
    gitrev="$1"
    git -C "$repoDir" checkout "$gitrev"
    matches=$(
        for issue in $issues ; do
            echo "checking bug $issue" >&2
            affectedFiles=$(python3 "$codeDir/fetch_bugzilla_bugs/get_bugzilla_patches.py" 'files' $issue)
            for affectedFile in $affectedFiles; do
                if [ ! -f "$repoDir/$affectedFile" ]; then
                    echo "$issue"
                    break
                fi
            done
        done)
    echo '{"issues": ['
    unset comma
    for match in $matches ; do
        if [ -n "$comma" ]; then echo ","; fi
        jq ".issues | map(select(.key == \"-$match\")) | .[]" "$project/res0.json"
        comma=true
    done
    echo ']}'
}


# css/style
mkdir -p "$issuesDir/css-issues"
filter "$toFilter/css-issues" \
               'dom/animation/AnimValuesStyleRule.cpp' \
               'dom/animation/AnimValuesStyleRule.h' \
               'layout/base/GeckoRestyleManager.cpp' \
               'layout/base/GeckoRestyleManager.h' \
               'layout/base/RestyleTracker.cpp' \
               'layout/style/CSSStyleSheet.cpp' \
               'layout/style/CSSStyleSheet.h' \
               'layout/style/CSSVariableDeclarations.cpp' \
               'layout/style/CSSVariableDeclarations.h' \
               'layout/style/CSSVariableResolver.cpp' \
               'layout/style/CSSVariableResolver.h' \
               'layout/style/CSSVariableValues.cpp' \
               'layout/style/CSSVariableValues.h' \
               'layout/style/Declaration.cpp' \
               'layout/style/Declaration.h' \
               'layout/style/GeckoStyleContext.cpp' \
               'layout/style/GeckoStyleContext.h' \
               'layout/style/ImportRule.h' \
               'layout/style/IncrementalClearCOMRuleArray.cpp' \
               'layout/style/IncrementalClearCOMRuleArray.h' \
               'layout/style/NameSpaceRule.h' \
               'layout/style/RuleNodeCacheConditions.cpp' \
               'layout/style/RuleProcessorCache.cpp' \
               'layout/style/RuleProcessorCache.h' \
               'layout/style/StyleRule.cpp' \
               'layout/style/StyleRule.h' \
               'layout/style/nsCSSDataBlock.cpp' \
               'layout/style/nsCSSParser.cpp' \
               'layout/style/nsCSSRuleProcessor.cpp' \
               'layout/style/nsCSSRuleProcessor.h' \
               'layout/style/nsCSSRules.cpp' \
               'layout/style/nsIStyleRule.h' \
               'layout/style/nsIStyleRuleProcessor.h' \
               'layout/style/nsMediaList.cpp' \
               'layout/style/nsMediaList.h' \
               'layout/style/nsNthIndexCache.cpp' \
               'layout/style/nsRuleData.cpp' \
               'layout/style/nsRuleData.h' \
               'layout/style/nsRuleNode.cpp' \
               'layout/style/nsRuleNode.h' \
               'layout/style/nsRuleWalker.h' \
               'layout/style/nsStyleSet.cpp' \
               'layout/style/nsStyleSet.h' \
               'layout/base/RestyleManager.cpp' \
               'layout/base/RestyleManager.h' \
               'layout/style/nsCSSStyleSheet.cpp' \
               'layout/style/nsCSSStyleSheet.h' \
               'layout/style/nsCSSDeclaration.cpp' \
               'layout/style/nsCSSDeclaration.h' \
               'layout/style/nsICSSImportRule.h' \
               'layout/style/nsICSSNameSpaceRule.h' \
               'layout/style/nsCSSStyleRule.cpp' \
               'layout/style/nsICSSStyleRule.h' \
               'layout/style/nsIMediaList.h' \
               'layout/base/RestyleManagerBase.cpp' \
               'layout/style/AnimationCommon.cpp' \
               > "$issuesDir/css-issues/res0.json"

# layers
mkdir -p "$issuesDir/layers-issues"
filter "$toFilter/layers-issues" \
       'gfx/2d/CaptureCommandList.cpp' \
       'gfx/2d/CaptureCommandList.h' \
       'gfx/2d/DrawCommand.h' \
       'gfx/2d/DrawCommands.h' \
       'gfx/2d/DrawTargetCapture.cpp' \
       'gfx/2d/DrawTargetCapture.h' \
       'gfx/2d/DrawTargetDual.cpp' \
       'gfx/2d/DrawTargetDual.h' \
       'gfx/2d/DrawTargetTiled.cpp' \
       'gfx/2d/DrawTargetTiled.h' \
       'gfx/2d/DrawTargetWrapAndRecord.cpp' \
       'gfx/2d/DrawTargetWrapAndRecord.h' \
       'gfx/2d/FilterNodeCapture.cpp' \
       'gfx/2d/FilterNodeCapture.h' \
       'gfx/2d/PathCapture.cpp' \
       'gfx/2d/PathCapture.h' \
       'gfx/2d/SourceSurfaceCapture.cpp' \
       'gfx/2d/SourceSurfaceCapture.h' \
       'gfx/2d/SourceSurfaceDual.h' \
       'gfx/gl/SharedSurfaceGLX.cpp' \
       'gfx/gl/SharedSurfaceGLX.h' \
       'gfx/layers/apz/public/MetricsSharingController.h' \
       'gfx/layers/apz/test/gtest/InternalHitTester.cpp' \
       'gfx/layers/apz/test/gtest/InternalHitTester.h' \
       'gfx/layers/basic/AutoMaskData.h' \
       'gfx/layers/basic/BasicCanvasLayer.cpp' \
       'gfx/layers/basic/BasicCanvasLayer.h' \
       'gfx/layers/basic/BasicColorLayer.cpp' \
       'gfx/layers/basic/BasicCompositor.cpp' \
       'gfx/layers/basic/BasicCompositor.h' \
       'gfx/layers/basic/BasicContainerLayer.cpp' \
       'gfx/layers/basic/BasicContainerLayer.h' \
       'gfx/layers/basic/BasicImageLayer.cpp' \
       'gfx/layers/basic/BasicImages.cpp' \
       'gfx/layers/basic/BasicImplData.h' \
       'gfx/layers/basic/BasicLayerManager.cpp' \
       'gfx/layers/basic/BasicLayers.h' \
       'gfx/layers/basic/BasicLayersImpl.cpp' \
       'gfx/layers/basic/BasicLayersImpl.h' \
       'gfx/layers/basic/BasicPaintedLayer.cpp' \
       'gfx/layers/basic/BasicPaintedLayer.h' \
       'gfx/layers/basic/MacIOSurfaceTextureHostBasic.cpp' \
       'gfx/layers/basic/MacIOSurfaceTextureHostBasic.h' \
       'gfx/layers/basic/TextureClientX11.cpp' \
       'gfx/layers/basic/TextureClientX11.h' \
       'gfx/layers/basic/TextureHostBasic.cpp' \
       'gfx/layers/basic/TextureHostBasic.h' \
       'gfx/layers/basic/X11BasicCompositor.cpp' \
       'gfx/layers/basic/X11BasicCompositor.h' \
       'gfx/layers/basic/X11TextureSourceBasic.cpp' \
       'gfx/layers/basic/X11TextureSourceBasic.h' \
       'gfx/layers/client/ClientCanvasLayer.cpp' \
       'gfx/layers/client/ClientCanvasLayer.h' \
       'gfx/layers/client/ClientCanvasRenderer.cpp' \
       'gfx/layers/client/ClientCanvasRenderer.h' \
       'gfx/layers/client/ClientColorLayer.cpp' \
       'gfx/layers/client/ClientContainerLayer.cpp' \
       'gfx/layers/client/ClientContainerLayer.h' \
       'gfx/layers/client/ClientImageLayer.cpp' \
       'gfx/layers/client/ClientLayerManager.cpp' \
       'gfx/layers/client/ClientLayerManager.h' \
       'gfx/layers/client/ClientPaintedLayer.cpp' \
       'gfx/layers/client/ClientPaintedLayer.h' \
       'gfx/layers/client/ClientReadbackLayer.h' \
       'gfx/layers/client/ClientTiledPaintedLayer.cpp' \
       'gfx/layers/client/ClientTiledPaintedLayer.h' \
       'gfx/layers/client/ContentClient.cpp' \
       'gfx/layers/client/ContentClient.h' \
       'gfx/layers/client/MultiTiledContentClient.cpp' \
       'gfx/layers/client/MultiTiledContentClient.h' \
       'gfx/layers/client/SingleTiledContentClient.cpp' \
       'gfx/layers/client/SingleTiledContentClient.h' \
       'gfx/layers/client/TiledContentClient.cpp' \
       'gfx/layers/client/TiledContentClient.h' \
       'gfx/layers/composite/AsyncCompositionManager.cpp' \
       'gfx/layers/composite/AsyncCompositionManager.h' \
       'gfx/layers/composite/CanvasLayerComposite.cpp' \
       'gfx/layers/composite/CanvasLayerComposite.h' \
       'gfx/layers/composite/ColorLayerComposite.cpp' \
       'gfx/layers/composite/ColorLayerComposite.h' \
       'gfx/layers/composite/ConsolasFontData.h' \
       'gfx/layers/composite/ContainerLayerComposite.cpp' \
       'gfx/layers/composite/ContainerLayerComposite.h' \
       'gfx/layers/composite/ContentHost.cpp' \
       'gfx/layers/composite/ContentHost.h' \
       'gfx/layers/composite/Diagnostics.cpp' \
       'gfx/layers/composite/FPSCounter.cpp' \
       'gfx/layers/composite/FPSCounter.h' \
       'gfx/layers/composite/ImageHost.cpp' \
       'gfx/layers/composite/ImageHost.h' \
       'gfx/layers/composite/ImageLayerComposite.cpp' \
       'gfx/layers/composite/ImageLayerComposite.h' \
       'gfx/layers/composite/LayerManagerComposite.cpp' \
       'gfx/layers/composite/LayerManagerComposite.h' \
       'gfx/layers/composite/LayerManagerCompositeUtils.h' \
       'gfx/layers/composite/PaintCounter.cpp' \
       'gfx/layers/composite/PaintCounter.h' \
       'gfx/layers/composite/PaintedLayerComposite.cpp' \
       'gfx/layers/composite/PaintedLayerComposite.h' \
       'gfx/layers/composite/TextRenderer.cpp' \
       'gfx/layers/composite/TextRenderer.h' \
       'gfx/layers/composite/TiledContentHost.cpp' \
       'gfx/layers/composite/TiledContentHost.h' \
       'gfx/layers/composite/X11TextureHost.cpp' \
       'gfx/layers/composite/X11TextureHost.h' \
       'gfx/layers/d3d11/BlendingHelpers.hlslh' \
       'gfx/layers/d3d11/mlgshaders/blend-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/blend-ps-generated.hlslh' \
       'gfx/layers/d3d11/mlgshaders/blend-ps-generated.hlslh.tpl' \
       'gfx/layers/d3d11/mlgshaders/blend-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/blend-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/clear-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/clear-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/clear-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/color-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/color-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/color-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/common-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/common-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/component-alpha-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/diagnostics-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/diagnostics-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/diagnostics-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/mask-combiner-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/mask-combiner-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/mask-combiner-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/test-features-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/textured-common.hlsl' \
       'gfx/layers/d3d11/mlgshaders/textured-ps.hlsl' \
       'gfx/layers/d3d11/mlgshaders/textured-vs.hlsl' \
       'gfx/layers/d3d11/mlgshaders/ycbcr-ps.hlsl' \
       'gfx/layers/d3d11/ReadbackManagerD3D11.cpp' \
       'gfx/layers/d3d11/ReadbackManagerD3D11.h' \
       'gfx/layers/DirectedGraph.h' \
       'gfx/layers/ImageLayers.cpp' \
       'gfx/layers/ImageLayers.h' \
       'gfx/layers/ipc/LayerTransactionChild.cpp' \
       'gfx/layers/ipc/LayerTransactionChild.h' \
       'gfx/layers/ipc/LayerTransactionParent.cpp' \
       'gfx/layers/ipc/LayerTransactionParent.h' \
       'gfx/layers/ipc/ShadowLayers.cpp' \
       'gfx/layers/ipc/ShadowLayers.h' \
       'gfx/layers/ipc/ShadowLayerUtilsMac.cpp' \
       'gfx/layers/ipc/ShadowLayerUtilsX11.cpp' \
       'gfx/layers/ipc/ShadowLayerUtilsX11.h' \
       'gfx/layers/LayerManager.cpp' \
       'gfx/layers/LayerMetricsWrapper.h' \
       'gfx/layers/LayerScope.cpp' \
       'gfx/layers/LayerScope.h' \
       'gfx/layers/Layers.cpp' \
       'gfx/layers/Layers.h' \
       'gfx/layers/LayersHelpers.cpp' \
       'gfx/layers/LayersHelpers.h' \
       'gfx/layers/LayerSorter.cpp' \
       'gfx/layers/LayerSorter.h' \
       'gfx/layers/LayerTreeInvalidation.cpp' \
       'gfx/layers/LayerTreeInvalidation.h' \
       'gfx/layers/opengl/GLBlitTextureImageHelper.cpp' \
       'gfx/layers/opengl/GLBlitTextureImageHelper.h' \
       'gfx/layers/opengl/X11TextureSourceOGL.cpp' \
       'gfx/layers/opengl/X11TextureSourceOGL.h' \
       'gfx/layers/PaintThread.cpp' \
       'gfx/layers/PaintThread.h' \
       'gfx/layers/protobuf/LayerScopePacket.pb.h' \
       'gfx/layers/ReadbackProcessor.cpp' \
       'gfx/layers/ReadbackProcessor.h' \
       'gfx/layers/RenderTrace.cpp' \
       'gfx/layers/RenderTrace.h' \
       'gfx/layers/RotatedBuffer.cpp' \
       'gfx/layers/RotatedBuffer.h' \
       'gfx/layers/SourceSurfaceVolatileData.cpp' \
       'gfx/layers/SourceSurfaceVolatileData.h' \
       'gfx/layers/TextureDIB.cpp' \
       'gfx/layers/TextureDIB.h' \
       'gfx/layers/TiledLayerBuffer.h' \
       'gfx/src/TiledRegion.cpp' \
       'gfx/src/TiledRegion.h' \
       'gfx/tests/gtest/TestCompositor.cpp' \
       'gfx/tests/gtest/TestLayers.h' \
       'gfx/tests/gtest/TestTextureCompatibility.cpp' \
       'gfx/thebes/gfxGdkNativeRenderer.cpp' \
       'gfx/thebes/gfxGdkNativeRenderer.h' \
       'gfx/thebes/gfxXlibNativeRenderer.cpp' \
       'gfx/thebes/gfxXlibNativeRenderer.h' \
       'layout/painting/FrameLayerBuilder.cpp' \
       'layout/painting/FrameLayerBuilder.h' \
       'widget/gtk/WindowSurfaceXRender.cpp' \
       'widget/gtk/WindowSurfaceXRender.h' \
       'gfx/2d/DrawTargetRecording.cpp' \
       'gfx/2d/DrawTargetRecording.h' \
       'gfx/gl/GLBlitTextureImageHelper.cpp' \
       'gfx/gl/GLBlitTextureImageHelper.h' \
       'gfx/layers/basic/BasicCanvasLayer.cpp' \
       'gfx/layers/basic/BasicCanvasLayer.h' \
       'gfx/layers/basic/BasicColorLayer.cpp' \
       'gfx/layers/basic/BasicContainerLayer.cpp' \
       'gfx/layers/basic/BasicContainerLayer.h' \
       'gfx/layers/basic/BasicImageLayer.cpp' \
       'gfx/layers/basic/BasicLayerManager.cpp' \
       'gfx/layers/basic/BasicLayers.cpp' \
       'gfx/layers/basic/BasicLayers.h' \
       'gfx/layers/basic/BasicThebesLayer.cpp' \
       'gfx/layers/basic/BasicThebesLayer.h' \
       'gfx/layers/BasicLayers.h' \
       'gfx/layers/basic/TextureHostX11.cpp' \
       'gfx/layers/basic/TextureHostX11.h' \
       'gfx/layers/client/ClientThebesLayer.cpp' \
       'gfx/layers/client/ClientThebesLayer.h' \
       'gfx/layers/client/ClientTiledThebesLayer.cpp' \
       'gfx/layers/client/ClientTiledThebesLayer.h' \
       'gfx/layers/client/TiledContentClient.cpp' \
       'gfx/layers/client/TiledContentClient.h' \
       'gfx/layers/composite/ThebesLayerComposite.cpp' \
       'gfx/layers/composite/ThebesLayerComposite.h' \
       'gfx/layers/ipc/CompositorParent.cpp' \
       'gfx/layers/ipc/CompositorParent.h' \
       'gfx/layers/ipc/LayerTransactionChild.cpp' \
       'gfx/layers/ipc/LayerTransactionChild.h' \
       'gfx/layers/ipc/ShadowLayersChild.cpp' \
       'gfx/layers/ipc/ShadowLayersChild.h' \
       'gfx/layers/ipc/ShadowLayersParent.cpp' \
       'gfx/layers/ipc/ShadowLayersParent.h' \
       'gfx/layers/opengl/FPSCounter.h' \
       'gfx/layers/ThebesLayerBuffer.cpp' \
       'gfx/layers/ThebesLayerBuffer.h' \
       'gfx/thebes/public/gfxGdkNativeRenderer.h' \
       'gfx/thebes/public/gfxXlibNativeRenderer.h' \
       'gfx/thebes/src/gfxGdkNativeRenderer.cpp' \
       'gfx/thebes/src/gfxXlibNativeRenderer.cpp' \
       'layout/base/FrameLayerBuilder.cpp' \
       'layout/base/FrameLayerBuilder.h' \
       'gfx/layers/d3d11/MLGDeviceD3D11.cpp' \
       'gfx/layers/d3d11/MLGDeviceD3D11.h' \
       'gfx/layers/mlgpu/BufferCache.cpp' \
       'gfx/layers/mlgpu/BufferCache.h' \
       'gfx/layers/mlgpu/CanvasLayerMLGPU.cpp' \
       'gfx/layers/mlgpu/CanvasLayerMLGPU.h' \
       'gfx/layers/mlgpu/ClearRegionHelper.h' \
       'gfx/layers/mlgpu/ContainerLayerMLGPU.cpp' \
       'gfx/layers/mlgpu/ContainerLayerMLGPU.h' \
       'gfx/layers/mlgpu/FrameBuilder.cpp' \
       'gfx/layers/mlgpu/FrameBuilder.h' \
       'gfx/layers/mlgpu/ImageLayerMLGPU.cpp' \
       'gfx/layers/mlgpu/ImageLayerMLGPU.h' \
       'gfx/layers/mlgpu/LayerManagerMLGPU.cpp' \
       'gfx/layers/mlgpu/LayerManagerMLGPU.h' \
       'gfx/layers/mlgpu/LayerMLGPU.cpp' \
       'gfx/layers/mlgpu/LayerMLGPU.h' \
       'gfx/layers/mlgpu/MaskOperation.cpp' \
       'gfx/layers/mlgpu/MaskOperation.h' \
       'gfx/layers/mlgpu/MemoryReportingMLGPU.cpp' \
       'gfx/layers/mlgpu/MemoryReportingMLGPU.h' \
       'gfx/layers/mlgpu/MLGDevice.cpp' \
       'gfx/layers/mlgpu/MLGDevice.h' \
       'gfx/layers/mlgpu/MLGDeviceTypes.h' \
       'gfx/layers/mlgpu/MLGPUScreenshotGrabber.cpp' \
       'gfx/layers/mlgpu/MLGPUScreenshotGrabber.h' \
       'gfx/layers/mlgpu/PaintedLayerMLGPU.cpp' \
       'gfx/layers/mlgpu/PaintedLayerMLGPU.h' \
       'gfx/layers/mlgpu/RenderPassMLGPU.cpp' \
       'gfx/layers/mlgpu/RenderPassMLGPU.h' \
       'gfx/layers/mlgpu/RenderPassMLGPU-inl.h' \
       'gfx/layers/mlgpu/RenderViewMLGPU.cpp' \
       'gfx/layers/mlgpu/RenderViewMLGPU.h' \
       'gfx/layers/mlgpu/ShaderDefinitionsMLGPU.h' \
       'gfx/layers/mlgpu/ShaderDefinitionsMLGPU-inl.h' \
       'gfx/layers/mlgpu/SharedBufferMLGPU.cpp' \
       'gfx/layers/mlgpu/SharedBufferMLGPU.h' \
       'gfx/layers/mlgpu/StagingBuffer.cpp' \
       'gfx/layers/mlgpu/StagingBuffer.h' \
       'gfx/layers/mlgpu/TexturedLayerMLGPU.cpp' \
       'gfx/layers/mlgpu/TexturedLayerMLGPU.h' \
       'gfx/layers/mlgpu/TextureSourceProviderMLGPU.cpp' \
       'gfx/layers/mlgpu/TextureSourceProviderMLGPU.h' \
       'gfx/layers/mlgpu/UtilityMLGPU.h' \
       'gfx/layers/LayerAttributes.h' \
       > "$issuesDir/layers-issues/res0.json"


# cubeb-linux
mkdir -p "$issuesDir/cubeb-linux-issues"
filter "$toFilter/cubeb-linux-issues" \
       'media/libcubeb/src/cubeb_pulse.c' \
       > "$issuesDir/cubeb-linux-issues/res0.json"

# cubeb-macos
mkdir -p "$issuesDir/cubeb-macos-issues"
filter "$toFilter/cubeb-macos-issues" \
       'media/libcubeb/src/cubeb_audiounit.c' \
       'media/libcubeb/src/cubeb_audiounit.cpp' \
       > "$issuesDir/cubeb-macos-issues/res0.json"

# prefs-parser
mkdir -p "$issuesDir/prefs-parser-issues"
filter "$toFilter/prefs-parser-issues" \
       'modules/libpref/src/nsPrefService.cpp' \
       'modules/libpref/src/Preferences.cpp' \
       'modules/libpref/Preferences.cpp' \
       > "$issuesDir/prefs-parser-issues/res0.json"

# cert-blocklist
mkdir -p "$issuesDir/cert-blocklist-issues"
filter "$toFilter/cert-blocklist-issues" \
       'security/manager/boot/src/CertBlocklist.cpp' \
       'security/manager/ssl/CertBlocklist.cpp' \
       > "$issuesDir/cert-blocklist-issues/res0.json"

# japanese-encoding
mkdir -p "$issuesDir/japanese-encoding-issues"
filter "$toFilter/japanese-encoding-issues" \
       'extensions/universalchardet/src/base/CharDistribution.cpp' \
       'extensions/universalchardet/src/base/JpCntx.cpp' \
       'extensions/universalchardet/src/base/nsCharSetProber.cpp' \
       'extensions/universalchardet/src/base/nsEUCJPProber.cpp' \
       'extensions/universalchardet/src/base/nsEscCharsetProber.cpp' \
       'extensions/universalchardet/src/base/nsEscSM.cpp' \
       'extensions/universalchardet/src/base/nsMBCSGroupProber.cpp' \
       'extensions/universalchardet/src/base/nsMBCSSM.cpp' \
       'extensions/universalchardet/src/base/nsSJISProber.cpp' \
       'extensions/universalchardet/src/base/nsUTF8Prober.cpp' \
       'extensions/universalchardet/src/base/nsUniversalDetector.cpp' \
       'extensions/universalchardet/src/xpcom/nsUdetXPCOMWrapper.cpp' \
       > "$issuesDir/japanese-encoding-issues/res0.json"

# language-identifier
mkdir -p "$issuesDir/language-identifier-issues"
filter "$toFilter/language-identifier-issues" \
       'intl/locale/MozLocale.cpp' \
       > "$issuesDir/language-identifier-issues/res0.json"

# language-negotiation
mkdir -p "$issuesDir/language-negotiation-issues"
filter "$toFilter/language-negotiation-issues" \
       'intl/locale/LocaleService.cpp' \
       > "$issuesDir/language-negotiation-issues/res0.json"

# encoding-detector
mkdir -p "$issuesDir/encoding-detector-issues"
filter "$toFilter/encoding-detector-issues" \
       'intl/chardet/' \
       > "$issuesDir/encoding-detector-issues/res0.json"

# hyphenation
mkdir -p "$issuesDir/hyphenation-issues"
filter "$toFilter/hyphenation-issues" \
       'intl/hyphenation/' \
       > "$issuesDir/hyphenation-issues/res0.json"
