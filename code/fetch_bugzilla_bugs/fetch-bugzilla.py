""" Fetch issues that match given jql query """
__author__ = "Kristian Berg"
__copyright__ = "Copyright (c) 2018 Axis Communications AB"
__license__ = "MIT"

from urllib.parse import quote
from urllib3 import PoolManager

import json
import os
import argparse
import io


def translate_json(j):
    j['issues'] = j.pop('bugs')
    for i in j['issues']:
        i['key'] = '-' + str(i.pop('id'))
        i['fields'] = {'created': i.pop('creation_time').replace('Z', ' +0000'),
                       'resolutiondate': i.pop('cf_last_resolved').replace('Z', '+0000')}


def fetch(keys, bugzilla_project_name):
    """ Fetch issues that match given bugzilla query """
    # Bugzilla query with necessary fields for fixed bugs
    q = 'include_fields=id,creation_time,cf_last_resolved' \
        + '&resolution=fixed'
    for key in keys:
        q += '&' + quote(key, safe='=')

    start_at = 0

    # max_results parameter is capped at 1000, specifying a higher value will
    # still return only the first 1000 results
    max_results = 1000

    os.makedirs('issues/', exist_ok=True)
    request = 'https://' + bugzilla_project_name + '/rest/bug?' + q \
        + '&startAt={}&maxResults=' + str(max_results)

    http = PoolManager()

    print('Progress: | = ' + str(max_results) + ' issues')
    while True:
        rurl = request.format(start_at)
        print(rurl)
        res = http.request('GET', request, retries=10)
        j = json.loads(res.data)
        translate_json(j)
        with io.open('issues/res' + str(start_at) + '.json', 'w',
                     encoding="utf-8") as f:
            json.dump(j, f)
        if len(j['issues']) < max_results:
            break
        print('|', end='', flush='True')
        start_at += max_results

    print('\nDone!')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="""Fetch issues from a Bugzilla REST API.
                                                 """)
    parser.add_argument('--bugzilla', type=str,
                        help="The name of the Bugzilla repository of the project (i.e. the domain).")
    parser.add_argument('--key-value', type=str, action='append',
                        help="Custom key-value string for Bugzilla query (MUST be a string of the form 'key=value'). You can find some of the options here: https://bugzilla.readthedocs.io/en/latest/api/core/v1/bug.html#search-bugs")
    #parser.add_argument('--products', type=str,
    #                    help="Comma-separated list of Prodcuts to query in (shorthand for multiple '--key-value product=Foo' options).")

    args = parser.parse_args()
    keys = args.key_value # + ['product=' + p for p in args.products.split(',')]
    bugzilla_project_name = args.bugzilla
    fetch(keys, bugzilla_project_name)
