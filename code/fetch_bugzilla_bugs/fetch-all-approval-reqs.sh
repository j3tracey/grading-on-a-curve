issuesFile=$1
firefoxDir=$2
bugzillaDir=$3
codeDir=$4

gitDir="$firefoxDir/.git"

mkdir -p "$bugzillaDir/bugs"

bugs=$(jq 'keys' "$issuesFile" | grep -Eo '[0-9]+')

for bug in $bugs; do
    causes=$(python3 "$codeDir/fetch_bugzilla_bugs/get-approval-request-comment.py" $bug)
    if [ "$causes" ] ; then
        causes=$(echo $causes | tr -dc '0-9 ')
        # grep -v for long indentation because it's easier if we construct the "inducedBy" field ourselves
        grep -v '        ' "$bugzillaDir/template.json" > "$bugzillaDir/bugs/$bug.json"
        sed -i "s/^    \"id\": \"\",/    \"id\": \"$bug\",/" "$bugzillaDir/bugs/$bug.json"
        comma=""
        for cause in $causes ; do
            causeRevs=""
            causeRevs=$(python3 "$codeDir/fetch_bugzilla_bugs/get_bugzilla_patches.py" 'titles' $cause |
                            while read -r line ; do
                                rev=$(git --git-dir "$gitDir" log --grep="$line" -F --pretty='tformat:"%H"' | tr '\n' ',' | head -c -1)
                                if [ "$rev" ] ; then
                                    echo "$rev"
                                fi
                            done | tr '\n' ',' | head -c -1)
            sed -i "s/^    ]/$comma        {\n            \"id\": \"$cause\",\n            \"revisions\": [$causeRevs]\n        }\n    ]/" "$bugzillaDir/bugs/$bug.json"
            comma=","
        done
    fi
done
