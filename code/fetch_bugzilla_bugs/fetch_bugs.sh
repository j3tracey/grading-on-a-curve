#!/bin/sh

## Fetches security bugs from Bugzilla.
## These queries are particular; modifications are made by editing this file,
## the only arguments are the path to this project's code (to call a helper
## script), and the path of where the issues should be output.

codeDir="$1"
issuesDir="$2"
fetchScript="$codeDir/fetch_bugzilla_bugs/fetch-bugzilla.py"
toFilter="$issuesDir/to_filter/"
#products='Core,Firefox,Firefox for Android,Firefox for iOS,Firefox OS,Focus,Focus-iOS,NSS,Privacy,Servo'
securityLevels='keywords=sec-critical, sec-high, sec-moderate, sec-low'

mkdir -p "$issuesDir"
mkdir -p "$toFilter"

echo "mp4 parsing (stagefright)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'short_desc=stagefright' \
        --key-value 'short_desc_type=allwordssubstr' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2018-01-09'
rm -rf "$issuesDir/stagefright-issues" && mv issues "$issuesDir/stagefright-issues"

echo "unicode encoder (uconv)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Internationalization' \
        --key-value 'longdesc=uconv' \
        --key-value 'longdesc_type=allwordssubstr' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2017-06-13'
rm -rf "$issuesDir/uconv-issues" && mv issues "$issuesDir/uconv-issues"

echo "CSS"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=CSS Parsing and Computation' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2017-11-14' \
        --key-value 'o1=anywords' --key-value 'n1=1' --key-value 'f1=short_desc' --key-value 'v1=stylo'
rm -rf "$toFilter/css-issues" && mv issues "$toFilter/css-issues"

# component interaction (XPCOM)
## This doesn't seem valid, so removing until/unless it's clear there's
## something comparable.
#python3 "$fetchScript" \
#        --bugzilla bugzilla.mozilla.org \
#        --key-value "$securityLevels" \
#        --key-value 'keywords_type=anywords' \
#        --key-value 'component=XPCOM'
#rm -rf "$issuesDir/xpcom-issues" && mv issues "$issuesDir/xpcom-issues"

echo "audio (cubeb)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Audio/Video: cubeb' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2020-01-17'
rm -rf "$toFilter/cubeb-linux-issues" && cp -r issues "$toFilter/cubeb-linux-issues"
rm -rf "$toFilter/cubeb-macos-issues" && mv issues "$toFilter/cubeb-macos-issues"

echo "prefrences parsing"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Preferences: Backend' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2018-02-01'
rm -rf "$toFilter/prefs-parser-issues" && mv issues "$toFilter/prefs-parser-issues"

echo "rendering (layers)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Graphics: Layers' \
        --key-value 'component=Graphics'
rm -rf "$toFilter/layers-issues" && mv issues "$toFilter/layers-issues"

echo "renering (webrender)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Graphics: WebRender'
rm -rf "$issuesDir/webrender-nonrust-issues" && mv issues "$issuesDir/webrender-nonrust-issues"

echo "certificate blocklist"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component= Security: PSM'
rm -rf "$toFilter/cert-blocklist-issues" && mv issues "$toFilter/cert-blocklist-issues"

echo "Japanese encoding detector"
echo "Unicode language identifier"
echo "language negotiation"
echo "encoding detector"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Internationalization' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2019-12-12'
rm -rf "$toFilter/japanese-encoding-issues" && cp -r issues "$toFilter/japanese-encoding-issues"
rm -rf "$toFilter/language-identifier-issues" && cp -r issues "$toFilter/language-identifier-issues"
rm -rf "$toFilter/language-negotiation-issues" && cp -r issues "$toFilter/language-negotiation-issues"
rm -rf "$toFilter/encoding-detector-issues" && mv issues "$toFilter/encoding-detector-issues"

echo "hyphenation (libhyphen)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Layout: Text and Fonts' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2019-11-12'
rm -rf "$toFilter/hyphenation-issues" && mv issues "$toFilter/hyphenation-issues"

echo "color management (qcms)"
python3 "$fetchScript" \
        --bugzilla bugzilla.mozilla.org \
        --key-value "$securityLevels" \
        --key-value 'keywords_type=anywords' \
        --key-value 'component=Graphics: Color Management' \
        --key-value 'chfieldvalue=FIXED' \
        --key-value 'chfield=resolution' \
        --key-value 'chfieldto=2020-09-21'
rm -rf "$issuesDir/qcms-issues" && mv issues "$issuesDir/qcms-issues"
