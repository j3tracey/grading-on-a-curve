#!/bin/env bash

project="$1"
issuesFile="$2"
resultsFile="$3"
handMatched="$4"
bugToIntroDir="$5"

bugs=$(grep "^$project	" "$handMatched"| cut -f2)
for bug in $bugs ; do
    fix=$(jq -r ".[\"-$bug\"].hash" "$issuesFile")
    blamed=$(jq "unique | .[] | select(.[0] == \"$fix\")[1]" "$resultsFile" | tr -d '"')
    groundTruth=$(grep -E "^$project	$bug" "$handMatched" | cut -f 5 | tr , ' ')
    match=0
    for commit in $blamed ; do
        if grep -q "$commit" <(echo $groundTruth) ; then ((match++)) ; fi
    done
    szz=$(echo "$blamed" | wc -w)
    form=$(echo "$groundTruth" | wc -w)
    echo "$bug,$szz,$match,$form"
done
