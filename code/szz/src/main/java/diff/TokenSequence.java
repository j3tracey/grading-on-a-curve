package diff;

import org.eclipse.jgit.diff.Sequence;
import net.sourceforge.pmd.lang.cpp.ast.Token;

import java.util.List;

public class TokenSequence extends Sequence {
    public List<Token> tokens;
    public TokenSequence(List<Token> tokens) {
        this.tokens = tokens;
    }

    @Override
    public int size() {
        return tokens.size();
    }
}
