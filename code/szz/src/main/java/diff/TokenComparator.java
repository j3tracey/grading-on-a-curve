package diff;

import org.eclipse.jgit.diff.SequenceComparator;
import net.sourceforge.pmd.lang.cpp.ast.Token;

import java.util.List;

public class TokenComparator extends SequenceComparator<TokenSequence> {
    @Override
    public boolean equals(TokenSequence a, int ai, TokenSequence b, int bi) {
        return a.tokens.get(ai).image.equals(b.tokens.get(bi).image);
    }

    @Override
    public int hash(TokenSequence seq, int ptr) {
        return seq.tokens.get(ptr).image.hashCode();
    }
}
