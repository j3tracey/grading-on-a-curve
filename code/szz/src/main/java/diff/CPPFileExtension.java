package diff;

import net.sourceforge.pmd.lang.cpp.ast.CppParserConstants;
import net.sourceforge.pmd.lang.cpp.ast.Token;
import net.sourceforge.pmd.lang.cpp.CppTokenManager;

import org.eclipse.jgit.diff.HistogramDiff;
import org.eclipse.jgit.diff.Edit;

import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.io.Reader;
import java.io.IOException;

import org.slf4j.Logger;

public class CPPFileExtension {
  private CppTokenManager tokenManager;
  private List<Token> tokens;
  public CPPFileExtension(String filePath, Reader reader) {
    this.tokenManager = new CppTokenManager(reader);
  }

  private List<Token> getTokens() {
    if (tokens == null) {
      this.tokens = new ArrayList<Token>();
      for(Token token = (Token) tokenManager.getNextToken();
          token.kind != CppParserConstants.EOF;
          token = (Token) tokenManager.getNextToken()) {
        this.tokens.add(token);
      }
    }
    return this.tokens;
  }

  public Set<Integer> allLineNumbers() {
    List<Token> tokens = this.getTokens();
    Set<Integer> lines = new HashSet<>();
    for (Token t : tokens) {
      lines.add(t.beginLine);
    }
    return lines;
  }

  public Set<Integer> affectedLineNumbers(CPPFileExtension fileToCompare) {
    Set<Integer> affectedLines = new HashSet<>();
    TokenSequence tokens1 = new TokenSequence(this.getTokens());
    TokenSequence tokens2 = new TokenSequence(fileToCompare.getTokens());
    HistogramDiff diff = new HistogramDiff();
    List<Edit> tokdiff = diff.diff(new TokenComparator(), tokens1, tokens2);
    tokdiff.forEach(it -> {
        for (int i = it.getBeginB(); i <= it.getEndB(); i++) {
          Token t = tokens2.tokens.get(i);
          affectedLines.add(t.beginLine);
        }
      }
      );
    return affectedLines;
  }
}
