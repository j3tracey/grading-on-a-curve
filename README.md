This repository contains the scripts and data necessary to reproduce the results of the ["Grading on a Curve: How Rust can Facilitate New Contributors while Decreasing Vulnerabilities"](https://cs.uwaterloo.ca/~iang/pubs/gradingcurve-secdev23.pdf) paper.
The data should reproduce by installing the requirements, cloning the repo, and running the `reproduceResults.sh` script in the `code` directory.
Note that this will take some time, as it requires cloning seven additional git repositories (one of which is the very large Firefox git mirror), fetching data from the relatively slow Bugzilla API, and running gradient descent on a non-trivial amount of data.
The exact amount of time will depend on your network connection and CPU, but running it overnight might be a good idea.

The software requirements for running the `reproduceResults.sh` script are as follows:

 - git
 - bash
 - jq
 - gradle
 - java and javac
 - python3 with the following modules:
   - numpy scipy pyplot urllib3

The `reproduceResults.sh` is intended to be fairly human-readable for a shell script, and its source can be used to find descriptions of what each step is doing and the role of each directory and file.
